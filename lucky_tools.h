#ifndef LUCKY_TOOLS_H
#define LUCKY_TOOLS_H

#include <stdbool.h>

int askInt();
int askIntPositive();
int askIntRange();
int askIntGreater();
int askIntSmaller();
bool askYN();

#endif