#ifndef MORSE_H
#define MORSE_H

int check_args(int, char**);
char get_arg(int, char**);
void version();

#endif