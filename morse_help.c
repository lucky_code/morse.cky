#include <stdio.h>
#include "morse_help.h"
#include "lucky_tools.h"

extern char RED[];
extern char GREEN[];
extern char BLUE[];
extern char DEF[];

void help() {
    printf("\n\t%sLucky Morse%s v0.1\n\n", GREEN, DEF);
    printf("Easy morse encoding and decoding.\n\n");
    printf("Examples of use:\n");
    printf("\tmorse.cky lucky world!\t\t\t%sencode 'lucky world!' in morse%s\n", BLUE, DEF);
    printf("\tmorse.cky --encode lucky world!\t\t%sencode 'lucky world!' in morse%s\n", BLUE, DEF);
    printf("\tmorse.cky --decode --file foo.txt\t%sdecode the file 'foo.txt' to latin alphabet%s\n", BLUE, DEF);
    printf("('--encoding' by default if no valid options are given)\n\n");
    printf("  -a, --alphabet\tshow all available characters\n");
    printf("  -d, --decode\t\tdecode from morse to latin alphabet\n");
    printf("  -e, --encode\t\tencode from latin alphabet to morse\n");
    printf("  -f, --file\t\tuse a file\n");
    printf("  -h, --help\t\tprint this screen :-)\n");
    printf("  -v, --version\t\tshow software version\n");
}