#include <stdio.h>
#include "alphabet.h"
#include "lucky_tools.h"

extern char RED[];
extern char GREEN[];
extern char BLUE[];
extern char DEF[];

char ALPHA[26][2] = {"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m",
                     "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"};
char mALPHA[26][5] = {".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--",
                      "-.", "---", ".--.", "--.-",".-.", "...", "-","..-", "...-", ".--", "-..-", "-.--", "--.."};
char ALPHAspec[6][3] = {"à", "ç", "é", "è", "ñ", " "};
char mALPHAspec[6][6] = {".--.-", "-.-..", "..-..", ".-..-", "--.--", "/"};
char SYMB[18][2] = {".", ",", "?", "'", "!", "/", "(", ")", "&", ":", ";", "=", "+", "-", "_", "\"", "$", "@"};
char mSYMB[18][8] = {".-.-.-", "--..--", "..--..", ".----.", "-.-.--", "-..-.", "-.--.", "-.--.-", ".-...",
                     "---...", "-.-.-.", "-...-", ".-.-.", "-....-", "..--.-", ".-..-.", "...-..-", ".--.-."};
char DIGIT[10][2] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9"};
char mDIGIT[10][6] = {"-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----."};

void print_alphabet() {
    printf("%sregular alphabet:%s\n", BLUE, DEF);
    for(int i=0; i<26; i++)
        printf("  %s'%s%s%s'%s\t= %s%s%s\n", BLUE, DEF, ALPHA[i], BLUE, DEF, GREEN, mALPHA[i], DEF);
    printf("%sspecial characters:%s\n", BLUE, DEF);
    for(int i=0; i<6; i++)
        printf("  %s'%s%s%s'%s\t= %s%s%s\n", BLUE, DEF, ALPHAspec[i], BLUE, DEF, GREEN, mALPHAspec[i], DEF);
    printf("%ssymbols:%s\n", BLUE, DEF);
    for(int i=0; i<18; i++)
        printf("  %s'%s%s%s'%s\t= %s%s%s\n", BLUE, DEF, SYMB[i], BLUE, DEF, GREEN, mSYMB[i], DEF);
    printf("%sdigits:%s\n", BLUE, DEF);
    for(int i=0; i<10; i++)
        printf("  %s'%s%s%s'%s\t= %s%s%s\n", BLUE, DEF, DIGIT[i], BLUE, DEF, GREEN, mDIGIT[i], DEF);
}