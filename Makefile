RED="\e[31m"
GREEN="\e[32m"
BLUE="\e[34m"
DEF="\e[39m"
OK= "\t"$(GREEN)ok!$(DEF)

SRC=morse.c alphabet.c morse_help.c lucky_tools.c
OBJECTS=$(SRC:.c=.o)

morse.cky: $(OBJECTS)
	@echo -n $(BLUE)\> binding objects..$(DEF)
	@gcc -o $@ $^
	@echo $(OK)
	@echo $(GREEN)\> morse.cky created!$(DEF)

%.o: %.c
	@echo -n $(BLUE)\> compiling file $<..$(DEF)
	@gcc -o $@ -c $<
	@echo $(OK)

.PHONY: run
run:	morse.cky
	@echo $(GREEN)\> running morse.cky..$(DEF)
	@./morse.cky
	@echo $(GREEN)\> end of make run!$(DEF)

.PHONY: clean
clean:
	@echo -n $(BLUE)\> cleaning files..$(DEF)
	@rm -f *~ *.o morse.cky
	@echo $(OK)