#include <stdio.h>
#include <string.h>
#include "morse.h"
#include "alphabet.h"
#include "morse_help.h"
#include "lucky_tools.h"

extern char RED[];
extern char GREEN[];
extern char BLUE[];
extern char DEF[];

int main(int argc, char* argv[]) {
    char arg;
    if(check_args(argc, argv)) return 0;
    arg = get_arg(argc, argv);
    switch(arg) {
        case 'a':
            print_alphabet();
            break;
        case 'd':
            printf("decoding\n");
            printf("%swoops.. not coded yet :-/ be patient%s\n", BLUE, DEF);
            break;
        case 'e':
            printf("encoding\n");
            printf("%swoops.. not coded yet :-/ be patient%s\n", BLUE, DEF);
            break;
        case 'f':
            printf("file\n");
            printf("%swoops.. not coded yet :-/ be patient%s\n", BLUE, DEF);
            break;
        case 'h':
            help();
            break;
        case 'v':
            version();
            break;
        default:
            printf("encoding by default\n");
            printf("%swoops.. not coded yet :-/ be patient%s\n", BLUE, DEF);
            break;
    }
    return 0;
}

int check_args(int argc, char* argv[]) {
    if(argc<=1) {
        printf("%sno arguments found!%s\n", RED, DEF);
        printf("use '%s--help%s' option for more infos\n", BLUE, DEF);
        return -1;
    }
    return 0;
}

char get_arg(int argc, char* argv[]) {
    if(strcmp(argv[1], "-a")==0 || strcmp(argv[1], "--alphabet")==0) return 'a';
    if(strcmp(argv[1], "-d")==0 || strcmp(argv[1], "--decode")==0) return 'd';
    if((strcmp(argv[1], "-e")==0 || strcmp(argv[1], "--encode")==0) && argc>2) return 'e';
    if(strcmp(argv[1], "-f")==0 || strcmp(argv[1], "--file")==0) return 'f';
    if(strcmp(argv[1], "-h")==0 || strcmp(argv[1], "--help")==0) return 'h';
    if(strcmp(argv[1], "-v")==0 || strcmp(argv[1], "--version")==0) return 'v';
    return -1;
}

void version() {
    printf("\n\t\t%sLucky Morse%s v0.1\n\n", BLUE, DEF);
    printf("%s           _           _       ____ ____ ____\n", GREEN);
    printf("          | |  _  _ __| |___  |__  |__  |__  |\n");
    printf("%scoded by:%s | |_| || / _| / / || |/ /  / /  / /\n", DEF, GREEN);
    printf("          |____\\_._\\__|_\\_\\\\_, /_/  /_/  /_/\n");
    printf("                           |__/%s\n\n", DEF);
}